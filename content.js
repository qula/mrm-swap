$( document ).ready(function() {
    
  var checkExist = setInterval(function() {
	
    var targetNode = document.querySelector('.mr-widget-section')
    if (targetNode) {
      clearInterval(checkExist);
      swapMessage()    
            
      var config = {
        childList: true,
        subtree: true
      }

      var callback = function() {
        swapMessage()
        observer.disconnect()
      }

      var observer = new MutationObserver(callback)
      observer.observe(targetNode, config)
         
    }
  }, 1000);
});

function swapMessage() {
  if($("#merge-message-edit").length) {
    var mrmcommit = document.getElementById('merge-message-edit').value.split('\n')    
    if(!mrmcommit[0].indexOf("Merge branch ")) {
      $("#merge-message-edit").val("Merge - " + mrmcommit[2] + "\n\n" + mrmcommit[0] + "\n" + mrmcommit [4]).sendkeys(" ")
      console.log("MRM-SWAP was here.")      
    }

  }
}
